console.log("S22 Discussion");
console.log("------------------------------------");

/*[ARRAY METHODS] 
	-Javascript has built-in function and methods for arrays. This allows us to manipulate and access array items.
	-Array can be either mutated or iterated
	- Array "mutations" seek to modify the contents of an array while array "iteration" aim to evaluate and loop over each element. */

/*MUTATOR METHODS
	-Functions / methods that mutate or change an array
	-This manipulates the original array performing tasks such as adding an removing elements. */

	console.log("Mutator Methods");
	let fruits = [
		"Apple",
		"Orange",
		"Kiwi",
		"Dragon Fruit",
	];

	/* push()
		-- Adds an element at the end of an array AND returns the new array's length.
		Syntax:
			arrayName.push[newElement];
	*/
		console.log("=>Push()");
		console.log("Fruits array: ");
		console.log(fruits);

		//fruits[fruits.length] = "Mango"; // to add Mango

		let fruitslength = fruits.push("Mango");
		console.log('Size/length of fruits array: '+ fruits.length);
		console.log('Mutated array from push("Mango")');
		console.log(fruits);

		console.log(fruits.push("Avocado", "Guava")); //returns the new array's length
		console.log('Mutated array from push("Avocado", "Guava")');
		console.log(fruits);


		/*function addMultipleFruits(fruit1, fruit2, fruit3){
			fruits.push(fruit1, fruit2, fruit3);
			console.log(fruits);
		}
		addMultipleFruits("Durian", "Atis","Melon");*/

console.log("------------------------------------");
console.log("=>pop()");

/* pop()
	Removes the last element in an array and AND returns the removed element
*/

	let removedFruit = fruits.pop();
	console.log(removedFruit);
	console.log("Mutated Array");
	console.log(fruits);


console.log("------------------------------------");
console.log("=>unshift");
/* unshift
    - Add one or more elements at the beginning of an array.
    - Syntax:
        arrayName.unshift("elementA");
        arrayName.unshift("elementA", "elementB");
*/

	fruits.unshift("Lime", "Banana");
	console.log('Mutated Array from unshift ("Lime", "Banana")');
	console.log(fruits);

console.log("------------------------------------");
console.log("=>shift");

/* shift
	-Removes an element at the beginning of an array and returns the removed statement */

	let anotherFruit = fruits.shift();
	console.log(anotherFruit);
	console.log('Mutated Array from shift method: ');
	console.log(fruits);

console.log("------------------------------------");
console.log("=>splice()");

/* splice();
    - simulatanously removes an element from a specified index number and adds new elements.
    - Syntax:
        arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
     dc
indeces	 0		1			2 		3 		4				5			6
	['Banana', 'Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado']
*/
			   //s  //d  //elements to be added
	fruits.splice(1, 2, "Lime", "Cherry", "Tomato"); //the elements to be added are added through replacing from starting index
	console.log('Mutated Array from splice method: ');
	console.log(fruits);

/*	we can also use splice to remove/delete element/s
	fruits.splice(2,4);//will delete cherry and tomato
	console.log(fruits);*/

console.log("------------------------------------");
console.log("=>sort()");

/*sort
	-Rearranges the array elements in alphanumeric order
	-Syntax:
	    - arrayName.sort();
	*/

	fruits.sort();
	console.log('Mutated Array from sort method: ');
	console.log(fruits);

console.log("------------------------------------");
console.log("=>reverse()");

/* reverse();
    - Reverse the order of array elements
    - Syntax:
        arrayName.reverse();
*/

	fruits.reverse();
	console.log('Mutated Array from reverse method: ');
	console.log(fruits);


console.log("-----------");
console.log("-----------");
console.log("==Mutator Methods==");
console.log("-----------");

/* Non-mutator
	-Non-mutator methods are functions that do not modify or change an array after they're created. */

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];

console.log("=>indexOf()");
// indexOf()
/*
    - Returns the index of the first matching element found in an array.
    - If no match was found, the result will be -1.
    - The search process will bne done from the first element proceeding to the last element.
    - Syntax:
        arrayName.indexOf(searchValue);
        arrayName.indexOf(searchValue, fromIndex/startingIndex);
*/

	let firstIndex = countries.indexOf("PH");
	console.log("Result of index of ('PH'): " +firstIndex);

/*	let firstIndex = countries.indexOf("PH", 2); //with specified starting index
			//PH -element to add
			//2 - starting index
	console.log("Result of index of ('PH'): " +firstIndex);
*/

	let invalidCountry = countries.indexOf("BR");
	console.log("Result of index of ('BR'): " +invalidCountry);

console.log("------------------------------------");
console.log("=>lastIndexOf()");

	/* lastIndexOf()
	    - Returns the index number of the last matching element found in an array.
	    - The search from process will be done from the last element proceeding to the first element.
	    - Syntax:
	        arrayName.lastIndexOf(searchValue);
	        arrayName.lastIndexOF(searchValue, fromIndex/EndingIndex);
	*/

	let lastIndex = countries.lastIndexOf("PH");
	console.log("Result of lastIndexOf('PH'): " +lastIndex);

	let lastIndexStart = countries.lastIndexOf("PH", 4);
	console.log("Result of lastIndexOf ('PH'): " +lastIndexStart);

console.log("------------------------------------");
console.log("=>slice()");

// slice()
/*
    - Portions/slices element from an array AND returns a new array.
    - Syntax:
        arrayName.slice(startingIndex); //until the last element of the array.
        arrayName.slice(startingIndex, endingIndex);
*/

	console.log("Original countries array:");
    console.log(countries);

    //slicing off elements from specified index to the last element 

    let sliceArrayA = countries.slice(2);
//		0	   1 	2 	   3 	 4	   5	 6		7
//    ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE']
        console.log("Result from slice(2):"); //result: ['CAN', 'SG', 'TH', 'PH', 'FR', 'DE'] stored in slicaArrayA
        console.log(sliceArrayA);

    // Slicing off element from specified index to another index. But the specified last index is not included in the return.

        let sliceArrayB = countries.slice(2, 5); //2 -> 4 // ends with the last declared index minus one
        console.log("Result from slice(2, 5):"); //result: ['CAN', 'SG', 'TH']
        console.log(sliceArrayB);

    //Slicing off elements from the last element of an array
//		-8    -7    -6     -5    -4    -3   -2    -1
 //   ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE']
        let sliceArrayC = countries.slice(-3);
            console.log("Result from slice method:"); //result: ['PH', 'FR', 'DE']
            console.log(sliceArrayC);

console.log("------------------------------------");
console.log("=>toString()");

/*toString();
	-Returns an array as a string, separated by commas.
	-Syntax:
		arrayName.toString();
*/

	// ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];
	let stringArray = countries.toString();
	console.log("Result from toString method: ");
	console.log(stringArray); //result: US,PH,CAN,SG,TH,PH,FR,DE
	//console.log(stringArray[0]);
	console.log(typeof stringArray); //to check if the array is converted to string.